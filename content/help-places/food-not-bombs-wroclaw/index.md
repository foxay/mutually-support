---
title: Food not Bombs Wrocław, Jedzenie zamiast bomb
date: 2022-07-20
draft: false
region:
  - Wrocław
  - Dworzec Nadodrze
  - Śródmieście
  - 50-222
tags:
  - inicjatywa społeczna
categories:
  - wydawka
description: Ciepłe posiłki, napoje, odzież, produkty domowe
---

Dworzec Nadodrze - przystanek tramwajowy, Śródmieście 50-222 Wrocław

Czynne: sobota, od 10:00 przygotowywanie posiłku, od 15:00 wydawka

Posiłki wegańskie są wydawane w każdą sobotę o 15:00 na Dworcu Nadodrze (przystanek tramwajowy) we Wrocławiu.

https://www.facebook.com/fnbwro 