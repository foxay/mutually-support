---
title: Ogrzewalnia świętego Brata Alberta
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Gajowicka 62
  - Krzyki
  - 53-422
tags:
  - inicjatywa zakonna
  - ogrzewalnia
  - noclegownia
categories:
  - schronienie
description: Sezonowa ogrzewalnia
---

Gajowicka 62, Krzyki 53-422 Wrocław

Telefon: +48 530 712 650

E-mail: wroclaw-ogrzewalnia@tpba.pl

Czynne: UZUPEŁNIĆ

Ogrzewalnia działa corocznie w czasie zimowym (od późnej jesieni do wczesnej wiosny). Poza okresem chłodów i mrozów placówka funkcjonuje jedynie w trybie informacyjnym i nie oferuje możliwości pobytu.

https://www.bratalbert.wroclaw.pl/ogrzewalnia-we-wroclawiu 
