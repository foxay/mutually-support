---
title: Zgromadzenie Sióstr św. Jadwigi we Wrocławiu
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Sępa-Szarzyńskiego Mikołaja 29
  - Śródmieście
  - 50-351
tags:
  - instytucja kościelna
categories:
  - jadłodajnia
description: Pieczywo i herbata
---

Sępa-Szarzyńskiego Mikołaja 29, Śródmieście 50-351 Wrocław

Telefon: +48 713 722 813, +48 713 223 461

Czynne: UZUPEŁNIĆ

Wydawanie żywności potrzebującym: kilkadziesiąt porcji chleba i ciepła herbata.

http://jadwizanki.com.pl/o-nas/apostolstwo