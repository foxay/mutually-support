---
title: Ogrzewalnia świętego Brata Alberta
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Małachowskiego 15
  - Krzyki
  - 50-084
tags:
  - inicjatywa zakonna
  - ogrzewalnia
  - noclegownia
categories:
  - schronienie
description: Sezonowa ogrzewalnia
---

Małachowskiego 15, Krzyki 50-084 Wrocław

Telefon: 71 717 34 25

E-mail: wroclaw-noclegownia@tpba.pl

Czynne: całodobowo

Schronienie, dostęp do kuchni, pralnia, łaźnia, świetlica z TV

