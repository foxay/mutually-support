---
title: Jadłodajnia CARITAS Centrum Socjalne Archidiecezji Wrocławskiej
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Słowiańska 17
  - Nadodrze
  - 50-234
tags:
  - instytucja kościelna
  - caritas
categories:
  - jadłodajnia
description: Ciepłe posiłki
---

ul. Słowiańska 17, Nadodrze 50-234 Wrocław 

Telefon: +48 713 721 986

Czynne: UZUPEŁNIĆ

Talon na posiłek można odebrać w oddziale Miejskiego Ośrodka Pomocy Społecznej przy ulicy Strzegomskiej 6. Z posiłków wydawanych przez wrocławską jadłodajnie w pierwszej kolejności korzystają osoby posiadające abonament wydany przez Miejski Ośrodek Pomocy Społecznej oraz Parafialne Zespoły Caritas.
